import base_test

class TestCases(base_test.BaseTest):
    def test_01_test_login_page_login(self):
        # step 1: Click on Login and the login Page Loads
        assert self.welcomepage.click_login(self.loginpage)
        # Step 2: Login to the webpage and confirm the main page appears
        assert self.loginpage.login(self.mainpage, self.param.username, self.param.password)
        # step 3: log out of the website and the welcome page appears
        assert self.mainpage.click_logout(self.welcomepage)

